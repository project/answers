INTRODUCTION
------------

Answers is a Q&A module which aims to provide the same features as
StackOverflow.com


REQUIREMENTS
------------

This module requires no modules outside of Drupal core and Answers.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will enable the dependencies.
