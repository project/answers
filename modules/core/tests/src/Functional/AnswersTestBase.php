<?php

namespace Drupal\Tests\answers_core\Functional;

use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Sets up answers comment & content types.
 */
abstract class AnswersTestBase extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

}
