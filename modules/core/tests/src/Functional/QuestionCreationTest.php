<?php

namespace Drupal\Tests\answers_core\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Create a node and test saving it.
 *
 * @group answers_core
 */
class QuestionCreationTest extends AnswersTestBase {

  use StringTranslationTrait;

  /**
   * Modules to enable.
   *
   * Enable dummy module that implements hook_ENTITY_TYPE_insert() for
   * exceptions (function node_test_exception_node_insert() ).
   *
   * @var array
   */
  protected static $modules = [
    'answers_core',
    'dblog',
  ];

  /**
   * The installation profile to use with this test.
   *
   * This test class requires the "tags" taxonomy field.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $web_user = $this->drupalCreateUser([
      'create answers_question content',
      'edit own answers_question content',
      'edit own comments',
      'access comments',
      'post comments',
    ]);
    $this->drupalLogin($web_user);
    $this->accessHandler = \Drupal::entityTypeManager()->getAccessControlHandler('node');
  }

  /**
   * Creates a "Question" node and verifies its consistency in the database.
   */
  public function testQuestionCreation() {
    $this->drupalGet('node/add/answers_question');
    $this->assertSession()->fieldNotExists('edit-revision');
    // Create a question node.
    $edit = [];
    $edit['title[0][value]'] = 'Woodchucks';
    $edit['body[0][value]'] = '<p>How much wood can a woodchuck chuck, if a woodchuck could chuck word.</p>';
    $edit['answers_tags[target_id]'] = 'drupal,phpunit';
    $this->submitForm($edit, $this->t('Save'));
    // Check that the Basic page has been created.
    $this->assertSession()->pageTextContains($this->t('@post @title has been created.', [
      '@post' => 'Question',
      '@title' => $edit['title[0][value]'],
    ]));
    // Verify that the creation message contains a link to a node.
    $view_link = $this->xpath('//div[@class="messages"]//a[contains(@href, :href)]', [':href' => 'node/']);
    $this->assertTrue(isset($view_link), 'The message area contains a link to a node');

    // Verify taxonomy tag links.
    $this->assertSession()->linkByHrefExists('taxonomy/term/1');
    $this->assertSession()->linkByHrefExists('taxonomy/term/2');

    // Check that the node exists in the database.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertNotEmpty($node, 'Node found in database.');
    // Verify that pages do not show submitted information by default.
    // $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextNotContains($node->getOwner()->getAccountName());
    $this->assertSession()->pageTextNotContains($this->container->get('date.formatter')->format($node->getCreatedTime()));

    // Answer the question.
    $edit = [];
    $edit['comment_body[0][value]'] = '<p>Exactly 42 cords of wood.</p>';
    $form_id = 'comment-form--2';
    $this->submitForm($edit, $this->t('Save'), $form_id);

    $this->assertSession()->pageTextContains('1 Answer');
    $edit = [];
    $edit['comment_body[0][value]'] = '<p>This comment is for the questions</p>';
    $form_id = 'comment-form';
    $this->submitForm($edit, $this->t('Save'), $form_id);
    $edit['comment_body[0][value]'] = '<p>This comment is for the an answer</p>';
    $form_id = 'comment-form--2';
    $this->submitForm($edit, $this->t('Save'), $form_id);

    $edit = [];
    $edit['comment_body[0][value]'] = '<p>The cords of wood are proportional to the density of the wood.</p>';
    $form_id = 'comment-form--3';
    $this->submitForm($edit, $this->t('Save'), $form_id);
    $this->assertSession()->pageTextContains('2 Answers');
  }

}
