(function ($) {
  Drupal.behaviors.answersCore = {
    attach: function answersCoreAttach(context, settings) {
      $('a.answers-question-comment-link', context).once('click', function (e) {
        console.log('Hello World');
        e.preventDefault();
      });
      $('a.answers-question-comment-link', context).click(function (e) {
        console.log('Hello World');
        e.preventDefault();
        e.stopPropagation();
        console.log(this);
        console.log($(this).siblings('form'));
        $(this).siblings('form').show();
        $(this).hide();
      });

      $('.node__links .links.inline .comment-add a', context).click(function (e) {
        console.log('Hello World');
        e.preventDefault();
        e.stopPropagation();
        console.log(this);
        console.log($(this).closest('article').find('form'));
        $(this)
          .closest('article')
          .find('form.comment-answers-question-comment-form')
          .show();
        $(this).hide();
      });
    },
  };
})(jQuery);
